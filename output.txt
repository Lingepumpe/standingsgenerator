{{GroupTableSlot|{{player|race=r|george}}||place=1|win_m=3|lose_m=0|win_g=6|lose_g=0|diff=+6|bg=up}}
{{GroupTableSlot|{{player|race=r|ArcTimes}}||place=1|win_m=3|lose_m=0|win_g=6|lose_g=0|diff=+6|bg=up}}
{{GroupTableSlot|{{player|race=r|BlueFalcon}}||place=1|win_m=3|lose_m=0|win_g=6|lose_g=1|diff=+5|bg=up}}
{{GroupTableSlot|{{player|race=r|Cronus}}||place=2|win_m=2|lose_m=0|win_g=4|lose_g=0|diff=+4|bg=up}}
{{GroupTableSlot|{{player|race=r|Randir}}||place=2|win_m=2|lose_m=0|win_g=4|lose_g=1|diff=+3|bg=up}}
{{GroupTableSlot|{{player|race=r|Mano}}||place=2|win_m=2|lose_m=0|win_g=4|lose_g=1|diff=+3|bg=up}}
{{GroupTableSlot|{{player|race=r|Frttt123}}||place=2|win_m=2|lose_m=0|win_g=4|lose_g=1|diff=+3|bg=up}}
{{GroupTableSlot|{{player|race=r|JuneBee}}||place=3|win_m=2|lose_m=1|win_g=5|lose_g=2|diff=+3|bg=up}}
{{GroupTableSlot|{{player|race=r|Bahram}}||place=3|win_m=2|lose_m=1|win_g=5|lose_g=3|diff=+2|bg=up}}
{{GroupTableSlot|{{player|race=r|why}}||place=4|win_m=2|lose_m=1|win_g=4|lose_g=2|diff=+2|bg=up}}
{{GroupTableSlot|{{player|race=r|Winawer}}||place=4|win_m=2|lose_m=1|win_g=4|lose_g=2|diff=+2|bg=up}}
{{GroupTableSlot|{{player|race=r|NoahtheWhale}}||place=4|win_m=2|lose_m=1|win_g=4|lose_g=2|diff=+2|bg=up}}
{{GroupTableSlot|{{player|race=r|Frivolus}}||place=4|win_m=2|lose_m=1|win_g=4|lose_g=2|diff=+2|bg=up}}
{{GroupTableSlot|{{player|race=r|Düsterwesen}}||place=4|win_m=2|lose_m=1|win_g=4|lose_g=2|diff=+2|bg=up}}
{{GroupTableSlot|{{player|race=r|Anthem}}||place=4|win_m=2|lose_m=1|win_g=4|lose_g=2|diff=+2|bg=up}}
{{GroupTableSlot|{{player|race=r|3oD(oG)}}||place=4|win_m=2|lose_m=1|win_g=4|lose_g=2|diff=+2|bg=up}}
{{GroupTableSlot|{{player|race=r|SirDubadoof}}||place=4|win_m=2|lose_m=1|win_g=4|lose_g=3|diff=+1|bg=up}}
{{GroupTableSlot|{{player|race=r|saviour}}||place=5|win_m=1|lose_m=0|win_g=2|lose_g=0|diff=+2|bg=up}}
{{GroupTableSlot|{{player|race=r|Frttt}}||place=5|win_m=1|lose_m=0|win_g=2|lose_g=0|diff=+2|bg=up}}
{{GroupTableSlot|{{player|race=r|Gwac7}}||place=6|win_m=1|lose_m=1|win_g=2|lose_g=2|diff=0|bg=up}}
{{GroupTableSlot|{{player|race=r|Kaboom}}||place=6|win_m=1|lose_m=1|win_g=2|lose_g=3|diff=-1|bg=down}}
{{GroupTableSlot|{{player|race=r|BobaLife}}||place=6|win_m=1|lose_m=1|win_g=2|lose_g=3|diff=-1|bg=down}}
{{GroupTableSlot|{{player|race=r|(B)lockhead}}||place=6|win_m=1|lose_m=1|win_g=2|lose_g=3|diff=-1|bg=down}}
{{GroupTableSlot|{{player|race=r|whiskeyjack}}||place=7|win_m=1|lose_m=2|win_g=3|lose_g=4|diff=-1|bg=down}}
{{GroupTableSlot|{{player|race=r|keiclone}}||place=7|win_m=1|lose_m=2|win_g=3|lose_g=4|diff=-1|bg=down}}
{{GroupTableSlot|{{player|race=r|Radivel-X17}}||place=7|win_m=1|lose_m=2|win_g=3|lose_g=4|diff=-1|bg=down}}
{{GroupTableSlot|{{player|race=r|DumbInACan}}||place=7|win_m=1|lose_m=2|win_g=3|lose_g=4|diff=-1|bg=down}}
{{GroupTableSlot|{{player|race=r|ConnectedV11}}||place=7|win_m=1|lose_m=2|win_g=3|lose_g=5|diff=-2|bg=down}}
{{GroupTableSlot|{{player|race=r|mLty}}||place=8|win_m=1|lose_m=2|win_g=2|lose_g=4|diff=-2|bg=down}}
{{GroupTableSlot|{{player|race=r|Siev}}||place=8|win_m=1|lose_m=2|win_g=2|lose_g=4|diff=-2|bg=down}}
{{GroupTableSlot|{{player|race=r|Kaduce}}||place=8|win_m=1|lose_m=2|win_g=2|lose_g=4|diff=-2|bg=down}}
{{GroupTableSlot|{{player|race=r|Deathstar42}}||place=8|win_m=1|lose_m=2|win_g=2|lose_g=4|diff=-2|bg=down}}
{{GroupTableSlot|{{player|race=r|KaboomBaby}}||place=9|win_m=0|lose_m=0|win_g=0|lose_g=0|diff=0|bg=up}}
{{GroupTableSlot|{{player|race=r|Blockhead}}||place=10|win_m=0|lose_m=1|win_g=1|lose_g=2|diff=-1|bg=down}}
{{GroupTableSlot|{{player|race=r|Annie98}}||place=10|win_m=0|lose_m=1|win_g=1|lose_g=2|diff=-1|bg=down}}
{{GroupTableSlot|{{player|race=r|MrChill}}||place=11|win_m=0|lose_m=1|win_g=0|lose_g=2|diff=-2|bg=down}}
{{GroupTableSlot|{{player|race=r|Glueburn}}||place=11|win_m=0|lose_m=1|win_g=0|lose_g=2|diff=-2|bg=down}}
{{GroupTableSlot|{{player|race=r|Fearius}}||place=11|win_m=0|lose_m=1|win_g=0|lose_g=2|diff=-2|bg=down}}
{{GroupTableSlot|{{player|race=r|Hibbina}}||place=12|win_m=0|lose_m=2|win_g=1|lose_g=4|diff=-3|bg=down}}
{{GroupTableSlot|{{player|race=r|こなた™}}||place=13|win_m=0|lose_m=2|win_g=0|lose_g=4|diff=-4|bg=down}}
{{GroupTableSlot|{{player|race=r|magicbullet}}||place=13|win_m=0|lose_m=2|win_g=0|lose_g=4|diff=-4|bg=down}}
{{GroupTableSlot|{{player|race=r|Sierr}}||place=14|win_m=0|lose_m=3|win_g=0|lose_g=6|diff=-6|bg=down}}
{{GroupTableSlot|{{player|race=r|DirePants}}||place=14|win_m=0|lose_m=3|win_g=0|lose_g=6|diff=-6|bg=down}}
{{GroupTableSlot|{{player|race=r|Cursedgamer}}||place=14|win_m=0|lose_m=3|win_g=0|lose_g=6|diff=-6|bg=down}}
