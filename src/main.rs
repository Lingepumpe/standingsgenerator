extern crate regex;

use regex::Regex;
use std::cmp::Ordering;
use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::io::{self, BufReader};

#[derive(Debug, Clone, Eq)]
struct StandingEntry {
    name: String,
    matches_won: u32,
    matches_lost: u32,
    games_won: u32,
    games_lost: u32,
    race: String,
}

impl StandingEntry {
    fn new(name: &str, race: &str) -> Self {
        StandingEntry {
            name: name.to_string(),
            matches_won: 0,
            matches_lost: 0,
            games_won: 0,
            games_lost: 0,
            race: race.to_string(),
        }
    }

    fn diff(&self) -> i32 {
        self.games_won as i32 - self.games_lost as i32
    }

    fn diffstr(&self) -> String {
        let diffnum = self.diff();
        if diffnum > 0 {
            "+".to_string() + &diffnum.to_string()
        } else {
            diffnum.to_string()
        }
    }

    fn tableline(&self, place: u32) -> String {
        //{{GroupTableSlot|{{player|race=p|KingPi}}||place=1|win_m=2|lose_m=0|win_g=4|lose_g=0|diff=+4|bg=up}}
        "{{GroupTableSlot|{{player|race=".to_string()
            + &self.race
            + "|"
            + &self.name
            + "}}||place="
            + &place.to_string()
            + "|win_m="
            + &self.matches_won.to_string()
            + "|lose_m="
            + &self.matches_lost.to_string()
            + "|win_g="
            + &self.games_won.to_string()
            + "|lose_g="
            + &self.games_lost.to_string()
            + "|diff="
            + &self.diffstr()
            + "|bg="
            + if self.diff() < 0 {
                // matches_won < self.matches_lost {
                "down"
            } else {
                "up"
            }
            + "}}\n"
    }
}

impl Ord for StandingEntry {
    fn cmp(&self, other: &StandingEntry) -> Ordering {
        if self.matches_won.cmp(&other.matches_won) != Ordering::Equal {
            return self.matches_won.cmp(&other.matches_won);
        }
        if other.matches_lost.cmp(&self.matches_lost) != Ordering::Equal {
            return other.matches_lost.cmp(&self.matches_lost);
        }
        if self.diff().cmp(&other.diff()) != Ordering::Equal {
            return self.diff().cmp(&other.diff());
        }
        if self.games_won.cmp(&other.games_won) != Ordering::Equal {
            return self.games_won.cmp(&other.games_won);
        }
        if other.games_lost.cmp(&self.games_lost) != Ordering::Equal {
            return other.games_lost.cmp(&self.games_lost);
        }
        other.name.to_lowercase().cmp(&self.name.to_lowercase())
    }
}

impl PartialOrd for StandingEntry {
    fn partial_cmp(&self, other: &StandingEntry) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for StandingEntry {
    fn eq(&self, other: &StandingEntry) -> bool {
        self.matches_won == other.matches_won
            && self.matches_lost == other.matches_lost
            && self.games_won == other.games_won
            && self.games_lost == other.games_lost
    }
}

fn main() -> io::Result<()> {
    let mut standing_map = HashMap::new();
    let f = File::open("inputgames2_t3.txt")?;
    let f = BufReader::new(f);

    let mut player_a = "".to_string();
    let mut player_b = "".to_string();
    let mut player_a_race = "".to_string();
    let mut player_b_race = "".to_string();
    let mut games_a = None;
    let mut games_b = None;

    for line in f.lines() {
        let line = line.unwrap();
        let re_p1 = Regex::new(r"\|player1=([^|]*)\|.*player1race=(\S*)\s*").unwrap();
        let re_p2 = Regex::new(r"\|player2=([^|]*)\|.*player2race=(\S*)\s*").unwrap();
        for cap in re_p1.captures_iter(&line) {
            player_a = cap[1]
                .to_string()
                .replace("(w.o)", "")
                .replace("(w.o.)", "")
                .trim()
                .to_string();
            player_a_race = cap[2].to_string();
            games_a = Some(0);
        }
        for cap in re_p2.captures_iter(&line) {
            player_b = cap[1]
                .to_string()
                .replace("(w.o)", "")
                .replace("(w.o.)", "")
                .trim()
                .to_string();
            player_b_race = cap[2].to_string();
            games_b = Some(0);
        }
        let re_mapwin1 = Regex::new(r"\|map\dwin=1").unwrap();
        let re_mapwin2 = Regex::new(r"\|map\dwin=2").unwrap();
        if re_mapwin1.is_match(&line) && games_a.is_some() {
            games_a = Some(games_a.unwrap() + 1);
        }
        if re_mapwin2.is_match(&line) && games_b.is_some() {
            games_b = Some(games_b.unwrap() + 1);
        }
        let re_double_close = Regex::new(r"}}").unwrap();
        if re_double_close.is_match(&line) && games_a.is_some() && games_b.is_some() {
            {
                let entry_a = standing_map
                    .entry(player_a.to_lowercase().clone())
                    .or_insert(StandingEntry::new(&player_a, &player_a_race));
                if games_a.unwrap() > games_b.unwrap() {
                    entry_a.matches_won += 1;
                } else if games_a.unwrap() < games_b.unwrap() {
                    entry_a.matches_lost += 1;
                }
                entry_a.games_won += games_a.unwrap();
                entry_a.games_lost += games_b.unwrap();
            }
            {
                let entry_b = standing_map
                    .entry(player_b.to_lowercase().clone())
                    .or_insert(StandingEntry::new(&player_b, &player_b_race));
                if games_a.unwrap() > games_b.unwrap() {
                    entry_b.matches_lost += 1;
                } else if games_a.unwrap() < games_b.unwrap() {
                    entry_b.matches_won += 1;
                }
                entry_b.games_won += games_b.unwrap();
                entry_b.games_lost += games_a.unwrap();
            }
            games_a = None;
            games_b = None;
        }
    }

    let mut sort_vec: Vec<_> = standing_map.iter().collect();
    sort_vec.sort_by(|a, b| b.1.cmp(a.1));
    let mut place = 1;
    let mut next_place = 1;
    let mut one_higher = None;
    let mut outfile = File::create("output.txt")?;

    for (_, v) in sort_vec {
        if let Some(higher) = one_higher {
            if !v.eq(&higher) {
                place = next_place;
            }
        }
        let tableline = v.tableline(place);
        next_place += 1;
        one_higher = Some(v.clone());
        outfile.write_all(tableline.as_bytes())?;
        print!("{}", tableline);
    }

    Ok(())
}
